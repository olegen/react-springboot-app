FROM node:16 as build

# Create /app/frontend for fron build
WORKDIR /app/frontend
COPY ./frontend/package.json .

# Install dependencies
RUN npm install --only=prod
COPY ./frontend .

# Build frontend at /app/frontend/build
RUN npm run build 

# Bundle static assets with nginx
FROM nginx:alpine
ENV NODE_ENV production
# Copy built assets from builder
COPY --from=build /app/frontend/build /usr/share/nginx/html
# Expose port
EXPOSE 80

HEALTHCHECK --interval=20s --timeout=3s \
  CMD curl -f http://localhost:80 || exit 1
# Start nginx
CMD ["nginx", "-g", "daemon off;"]
